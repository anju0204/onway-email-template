



(function ($) {
    $(document).ready(function () {
        $('.servi').owlCarousel({
                    loop:false,
                    margin:0,
                    smartSpeed: 1000,
                    responsiveClass:true,
                    responsive:{
                        0:{
                            items:1,
                            // nav:true,
                            autoHeight:false,
                             dots:false,
                            loop:true,
                            autoplay:true,
                            autoplayTimeout:3000,
                            autoplayHoverPause:false,
                        },
                        600:{
                            items:2,
                             dots:false,
                            // nav:true,
                          autoHeight:false
                        },
                          900:{
                            items:2,
                             dots:false,
                            // nav:true,
                          autoHeight:false
                        },
                        1000:{
                            items:4,
                            nav: false,
                            pagination: false,
                            dots:false,
                            loop:true,
                            autoplay:true,
                            autoplayTimeout:3000,
                            autoplayHoverPause:false,
                            
                        }
                    }
                })
     $('.testi').owlCarousel({
                    loop:false,
                    margin:0,
                    smartSpeed: 1000,
                    responsiveClass:true,
                    responsive:{
                        0:{
                            items:1,
                            // nav:true,
                            autoHeight:false,
                            dots:true,
                            loop:true,
                            autoplay:true,
                            autoplayTimeout:3000,
                            autoplayHoverPause:false,
                        },
                        600:{
                            items:1,
                            // nav:true,
                          autoHeight:false
                        },
                        1000:{
                            items:1,
                            // nav: true,
                            pagination: true,
                            dots:true,
                            loop:true,
                            autoplay:true,
                            autoplayTimeout:3000,
                            autoplayHoverPause:false,
                            
                        }
                    }
                })

      if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0)) {
            $(".popup-img").magnificPopup({
                type: "image",
                gallery: {
                    enabled: true,
                }
            });
            $(".popup-img-single").magnificPopup({
                type: "image",
                gallery: {
                    enabled: false,
                }
            });
            $('.popup-iframe').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                preloader: false,
                fixedContentPos: false
            });
        }


         //menubar scripts starts here
          var stickyNavTop = $('.nav1').offset().top;

                var stickyNav = function(){
                var scrollTop = $(window).scrollTop();

    if (scrollTop > stickyNavTop) { 
      $('.nav1').addClass('sticky');
       $('.nav1').addClass('w3-animate-top');
     $(".logosticky").css("width","60px");
    } else {
      $(".logosticky").css("width","auto");
      $('.nav1').removeClass('sticky'); 
      $('.nav1').removeClass('w3-animate-top');
    }
  };

  stickyNav();

  $(window).scroll(function() {
    stickyNav();
  });

  $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
  }
  var $subMenu = $(this).next(".dropdown-menu");
  $subMenu.toggleClass('show');


  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass("show");
  });


  return false;
});



 // flexslider
        $(window).load(function(){
    $('#carousel').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: true,
      pausePlay: false,
      mousewheel: true,
      slideshow: true,
      itemWidth: 150,
      itemMargin: 5,
      asNavFor: '#slider'
    });

    $('#slider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: true,
      slideshow: true,
      pausePlay: false,
      mousewheel: true,
      sync: "#carousel",
      start: function(slider){
        $('body').removeClass('loading');
      }
    });
    
  });
    // flexslider
         
 }); // End document ready

$(window).scroll(function () {
            if ($(this).scrollTop() != 0) {
                $(".scrollToTop").fadeIn();
            } else {
                $(".scrollToTop").fadeOut();
            }
        });

        $(".scrollToTop").click(function () {
            $("body,html").animate({ scrollTop: 0 }, 800);
        }); 
       

     $(document).ready(function(){
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
              scrollTop: $(hash).offset().top                             
            }, 1700, function(){
         
              // Add hash (#) to URL when done scrolling (default click behavior)
              window.location.hash = hash;
            });
          } // End if
        });
      });



     function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  document.getElementById(cityName).style.display = "block";  
}

// scrol
        //Scroll totop
        //-----------------------------------------------

})(this.jQuery);

jQuery(window).ready(function ($) {

});

jQuery(window).load(function ($) {
 // Isotope filters
        //-----------------------------------------------
        if ($('.isotope-container').length > 0 || $('.masonry-grid').length > 0 || $('.masonry-grid-fitrows').length > 0) {
            
                $('.masonry-grid').isotope({
                    itemSelector: '.masonry-grid-item',
                    layoutMode: 'masonry'
                });
                $('.masonry-grid-fitrows').isotope({
                    itemSelector: '.masonry-grid-item',
                    layoutMode: 'fitRows'
                });
        }
});

